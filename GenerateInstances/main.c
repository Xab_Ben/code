#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

//Generar una matriz de tamaño n con la estrategia Random.
void random_gen(double sp, double a, double b, int n){
    int i,j;
    double x;
    long int c;
    //Para cada posición en la matriz...
    for(i=0;i<n;i++){
      for(j=0;j<n;j++){
          //Generamos un número aleatorio entre 0 y 1.
          x = (double)rand() / (double)RAND_MAX;
          if(x<sp){
              //Si dicho número es menor que la sparsity (sp), el valor de la posición será cero.
              printf("0\t");
          }else{
              //Si no, el valor de la posición se calcula como max(1,(a*x)^b).
              x = (double)rand() / (double)RAND_MAX;
              c = (long int)pow(a*x,b);
              if(c<1) c=1;
              printf("%ld\t",c);
          }
      }
      printf("\n");
    }
}

//Generar una matriz de tamaño n con la estrategia Structured.
void str_gen(double d, double a, double b, int n){
  int i,j;
  double x;
  long int c;

  //Generamos n puntos aleatorios en una cuadrícula de tamaño 100x100.
  double *z = (double *) malloc(sizeof(double)*n);
  double *y = (double *) malloc(sizeof(double)*n);
  for(i=0;i<n;i++){
      z[i] = ((double)rand() / (double)RAND_MAX)*100;
      y[i] = ((double)rand() / (double)RAND_MAX)*100;
  }

  //Para cada posición en la matriz...
  for(i=0;i<n;i++){
    for(j=0;j<n;j++){
        //Calculamos la distancia euclidea entre los puntos i y j.
        x = sqrt(pow(z[j]-z[i],2)+pow(y[j]-y[i],2));
        if(x>d){
            //Si dicha distancia es mayor que cierta distancia (d), el valor de la posición será cero.
            printf("0\t");
        }else{
            //Si no, el valor de la posición se calcula como max(1,(a*x)^b).
            x = (double)rand() / (double)RAND_MAX;
            c = (long int)pow(a*x,b);
            if(c<1) c=1;
            printf("%ld\t",c);
        }
    }
    printf("\n");
  }
}

//Generar una matriz de tamaño n con la estrategia StructuredPlus.
void str_plus_gen(double d, double p, double a, double b, int n){
  int i,j;
  double x;
  long int c;

  //Generamos n puntos aleatorios en una cuadrícula de tamaño 100x100.
  double *z = (double *) malloc(sizeof(double)*n);
  double *y = (double *) malloc(sizeof(double)*n);
  for(i=0;i<n;i++){
      z[i] = ((double)rand() / (double)RAND_MAX)*100;
      y[i] = ((double)rand() / (double)RAND_MAX)*100;
  }

  //Para cada posición en la matriz...
  for(i=0;i<n;i++){
    for(j=0;j<n;j++){
        //Calculamos la distancia euclidea entre los puntos i y j.
        x = sqrt(pow(z[j]-z[i],2)+pow(y[j]-y[i],2));
        if(x>d){
            //Si dicha distancia es mayor que cierta distancia (d), generamos un número aleatorio entre 0 y 1.
            x = (double)rand() / (double)RAND_MAX;
            if(x<p){
                //Si dicho número es menor que cierta probabilidad (p), el valor de la posición se calcula como max(1,(a*x)^b).
                x = (double)rand() / (double)RAND_MAX;
                c = (long int)pow(a*x,b);
                if(c<1) c=1;
                printf("%ld\t",c);
            }else{
                //Si no, el valor de la posición será cero.
                printf("0\t");
            }
        }else{
            //Si no, el valor de la posición se calcula como max(1,(a*x)^b).
            x = (double)rand() / (double)RAND_MAX;
            c = (long int)pow(a*x,b);
            if(c<1) c=1;
            printf("%ld\t",c);
        }
    }
    printf("\n");
  }
}

//Función principal.
int main (int argc, char *argv[]) {

	  int i,j;
    double a1, b1, a2, b2;

    /*
      MATRIZ DE DISTANCIA
         1.- Mode_distance
         2.- Parameter1
            - sp si Mode_distance="Random"
            - d en caso contrario.
         3.- Parameter2
            - p si Mode_distance="StructuredPlus"
            - Ignorado en caso contrario.
         4.- a
         5.- b

      MATRIZ DE FLUJO
         6.- Mode_flow
         7.- Parameter1
             - sp si Mode_flow="Random"
             - d en caso contrario.
         8.- Parameter2
             - p si Mode_flow="StructuredPlus"
             - Ignorado en caso contrario.
         9.- a
         10.- b

      GENERAL
         11.- Size (Tamaño de la instancia)
         12.- Seed (Semilla para la aleatoriedad)
     */

    if(argc != 13){
      printf("Formato incorrecto.\n");
      exit(1);
    }

    srand(atoi(argv[12]));

    a1 = atof(argv[4]);
    b1 = atof(argv[5]);
    a2 = atof(argv[9]);
    b2 = atof(argv[10]);

    printf("%s\n\n",argv[11]);

    //Generamos la matriz de distancia con las características indicadas.
    if(strcmp(argv[1],"Random")==0){
        random_gen(atof(argv[2]), a1, b1, atoi(argv[11]));
    }else if(strcmp(argv[1],"Structured")==0){
        str_gen(atof(argv[2]), a1, b1, atoi(argv[11]));
    }else if(strcmp(argv[1],"StructuredPlus")==0){
        str_plus_gen(atof(argv[2]),atof(argv[3]), a1, b1, atoi(argv[11]));
    }

    printf("\n");

    //Generamos la matriz de flujo con las características indicadas.
    if(strcmp(argv[6],"Random")==0){
        random_gen(atof(argv[7]), a2, b2, atoi(argv[11]));
    }else if(strcmp(argv[6],"Structured")==0){
        str_gen(atof(argv[7]), a2, b2, atoi(argv[11]));
    }else if(strcmp(argv[6],"StructuredPlus")==0){
        str_plus_gen(atof(argv[7]), atof(argv[8]), a2, b2, atoi(argv[11]));
    }
}
