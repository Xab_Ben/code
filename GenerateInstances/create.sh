#Genera un benchmark de instancias con diversas características configurables.
for size in 50 60;
do
  num=1
  for ab1 in 100,1 10,2;
  do IFS=",";
    set -- $ab1;
    a1=$1
    b1=$2
    for ab2 in 100,1 10,2;
    do IFS=",";
      set -- $ab2;
      a2=$1
      b2=$2
      for mode_dist in "Random" "Structured" "StructuredPlus";
      do
        for mode_flow in "Random" "Structured" "StructuredPlus";
        do
          if [ $mode_dist == "Random" ];
          then
            par1=(0.25 0.5 0.75)
            par2=(0)
          elif [ $mode_dist == "Structured" ];
          then
            par1=(25 50 75)
            par2=(0)
          elif [ $mode_dist == "StructuredPlus" ];
          then
            par1=(25 50 75)
            par2=(0.1 0.2)
          fi
          if [ $mode_flow == "Random" ];
          then
            par3=(0.25 0.5 0.75)
            par4=(0)
          elif [ $mode_flow == "Structured" ];
          then
            par3=(25 50 75)
            par4=(0)
          elif [ $mode_flow == "StructuredPlus" ];
          then
            par3=(25 50 75)
            par4=(0.1 0.2)
          fi
          for p1 in ${par1[*]};
          do
            for p2 in ${par2[*]};
            do
              for p3 in ${par3[*]};
              do
                for p4 in ${par4[*]};
                do
                    ./GenerateInstances $mode_dist $p1 $p2 $a1 $b1 $mode_flow $p3 $p4 $a2 $b2 $size 0 > ./Generated/xab"$size"_"$num".dat
                    num=$(($num+1))
                done
              done
            done
          done
        done
      done
    done
  done
done
